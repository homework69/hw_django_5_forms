from django.db import models
from datetime import timedelta

from django.contrib.auth.models import User


class Product(models.Model):
    label = models.CharField(max_length=100, unique=True, blank=True, null=True)
    count = models.IntegerField(default=0, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    manufactured_date = models.DateField(blank=True, null=True)
    delivered_at = models.DateTimeField(auto_now_add=True, null=True)
    expiration_term = models.DurationField(blank=True, default=timedelta(days=365), null=True)
    used_weight = models.BooleanField(blank=True, null=True)
    weight = models.FloatField(blank=True, null=True)
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    web_ref = models.URLField(blank=True, name='Internet ref', null=True)
    product_uuid = models.UUIDField(editable=False, null=True)


class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)
    client_name = models.CharField(max_length=100, unique=True, null=True)
    client_email = models.EmailField(blank=True, null=True)
    client_ip = models.GenericIPAddressField(protocol="IPv4", null=True)
    personal_discount = models.DecimalField(blank=True, max_digits=4, decimal_places=2, null=True)


class ProductReview(models.Model):
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    picture = models.ImageField(blank=True, upload_to='images/%Y/%m/%d/')
    client_email = models.EmailField()
    rating = models.CharField(choices=[('1', 'Very bad'),
                                       ('2', 'Bad'),
                                       ('3', 'So-so'),
                                       ('4', 'Good'),
                                       ('5', 'Excellent')],
                              max_length=1)
    is_negative = models.BooleanField(blank=True)
    phone_number = models.CharField(blank=True, max_length=17)
