from django.shortcuts import render
from internet_market.forms import MyForm, MyForm1, FormFromModel, FormReview
from django.urls import reverse_lazy
from django.conf import settings
from django.views.generic.edit import FormView
from django.views.generic import ListView
from internet_market.models import Product
import os
from pathlib import PurePath


def my_form(request):
    form = MyForm(request.POST or None)

    if form.is_valid():
        print(form.cleaned_data)
    else:
        print(form.errors)

    return render(request, 'form_page.html', context={'form': form})


def my_form1(request):
    form = MyForm1(request.POST or None, request.FILES or None)

    if form.is_valid():
        print(form.cleaned_data)
        file_path = os.path.join(settings.MEDIA_ROOT, form.cleaned_data['profile_picture'].name)
        print(file_path)
        # print(PurePath.joinpath(settings.MEDIA_ROOT, form.cleaned_data['profile_picture'].name))
        with open(file_path, 'wb+') as temp_file:
            for chunk in form.cleaned_data['profile_picture']:
                temp_file.write(chunk)
    else:
        print(form.errors)

    return render(request, 'form_page1.html', context={'form': form})


class ModelFormView(FormView):
    form_class = FormFromModel
    template_name = 'form_page1.html'
    success_url = reverse_lazy('modelform')

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class ProductView(ListView):
    template_name = 'product_list.html'
    context_object_name = 'product_list'

    def get_queryset(self):
        return Product.objects.all()


class ProductReview(FormView):
    form_class = FormReview
    template_name = 'form_review.html'
    success_url = reverse_lazy('products')

    def get(self, request, product_id):
        form = FormReview(initial={'product_id': Product.objects.get(pk=product_id)})
        return render(request, 'form_review.html', context={'form': form})

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


"""
def product_review(request, product_id):
    form = FormReview
"""
