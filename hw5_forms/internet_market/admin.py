from django.contrib import admin
from internet_market.models import Client, Product, ProductReview


admin.site.register(Client)
admin.site.register(Product)
admin.site.register(ProductReview)

