from django.urls import path
from internet_market import views

urlpatterns = [
    path('try-form/', views.my_form, name='my_form'),
    path('try-form1/', views.my_form1, name='my_form1'),
    path('try-modelform/', views.ModelFormView.as_view(), name='modelform'),
    path('products/', views.ProductView.as_view(), name='products'),
    path('product-review/<int:product_id>', views.ProductReview.as_view(), name='product_review'),
]