from django import forms
from internet_market.models import Client, ProductReview


class MyForm(forms.Form):
    user_name = forms.CharField(initial='User',
                                error_messages={'required': 'Please enter your name'})
    email = forms.EmailField(error_messages={
        'required': 'Please enter your available email'})
    password = forms.CharField(max_length=16, min_length=8, widget=forms.PasswordInput)
    age = forms.IntegerField(required=False, help_text='Enter your current age')
    agreement = forms.BooleanField()
    user_birthday = forms.DateField(widget=forms.SelectDateWidget,
                                    required=False)
    gender = forms.ChoiceField(required=False,
                               choices=[('1', 'male'), ('2', 'female')])


class MyForm1(forms.Form):
    """
    With File and Image
    """
    user_name = forms.CharField(initial='User',
                                error_messages={'required': 'Please enter your name'})
    email = forms.EmailField(error_messages={
        'required': 'Please enter your available email'})
    password = forms.CharField(required=False, max_length=16, min_length=8, widget=forms.PasswordInput)
    age = forms.IntegerField(required=False, help_text='Enter your current age')
    agreement = forms.BooleanField()
    user_birthday = forms.DateField(widget=forms.SelectDateWidget,
                                    required=False)
    gender = forms.ChoiceField(required=False,
                               choices=[('1', 'male'), ('2', 'female')])
    profile_picture = forms.ImageField(required=False)


class FormFromModel(forms.ModelForm):
    class Meta:
        model = Client
        fields = '__all__'


class FormReview(forms.ModelForm):
    class Meta:
        model = ProductReview
        fields = '__all__'
